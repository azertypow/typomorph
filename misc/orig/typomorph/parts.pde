class Part{
  RPoint start;
  RPoint end;
  float vel_norm;
  float vel_norm2;
  
  RPoint vel;
  RPoint vel2;

  int birth;
  int lifetime;
  int life;
  
  int radial;
  boolean type_part;
  

  Part(RPoint[] line_in, int lifetime_in, int radial){

    birth = millis();
    life = millis() - birth;
    start = new RPoint(line_in[0]);
    end = new RPoint(line_in[1]);
    
    radial = radial;

    if (random(1) > 0.5){
      vel_norm = random(0.1, .05);
      type_part = false;
    }else{
      vel_norm = random(0.1, 1);
      type_part = true;
    }

    if (radial==0){
      type_part = true;
      vel_norm = random(1, 2);
    }else if(radial==1){
      vel_norm = random(0.1, .5);
      type_part = false;
    }

    vel = new RPoint(end);
    vel.sub(start);
    vel.norm();
    vel.scale(vel_norm);

    vel2 = new RPoint(vel);

    if (type_part){

      
      vel2.scale(random(1, 2));
    }
    
    lifetime = lifetime_in;

    
  }

  void update(){
    start.add(vel);
    end.add(vel2);
  }

  void draw(){
    // if (type_part){
    //   stroke(255);
    // }else{
      if (life<50){
        stroke(255);
      }else{
        stroke(255, map(life, 0, lifetime, 150, 0));
      }
    // }
    if (type_part){
      line(start.x, start.y, end.x, end.y);
    }else{
      RPoint d = new RPoint(end);
      d.sub(start);
      d.rotate(HALF_PI);
      d.add(start);
      line(start.x, start.y, d.x, d.y);
    }
  }
  
  boolean is_alive(){
    life = millis() - birth;
    if (life > lifetime){
      return false;
    }else{
      return true;
    }
  }
}
