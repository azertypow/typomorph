class EffectWaves extends Effect{
  RShape wave_ext;
  RShape wave_int;

  float rotate_num = 1.0;         // number of rotation accross the longest path 
  float rotate_speed = 0.0;       // rotation speed (rad/s)

  // length of the line in the two directions
  float amplitude_1 = 50;
  float amplitude_2 = -5;

  // mouse interactivity 
  boolean interactive = false;    // on/off
  float interactive_radius = 0.0; // radius of action 
                                  // the ratio is interpolated between the center of the edge
  float interactive_close = 1.0;  // ratio at the position the mouse
  float interactive_far = 0.0;    // ratio when outside of radius

  boolean draw_lines = true;

  // int last_update;
  // float angle = 0.0;

  EffectWaves (Typomorph typo_in){
    super(typo_in);
    //    last_update = millis();
  }

  EffectWaves (Typomorph typo_in, float amplitude_1_in, float amplitude_2_in){
    super(typo_in);
    //    last_update = millis();
    amplitude_1 = amplitude_1_in;
    amplitude_2 = amplitude_2_in;
  }

  EffectWaves (Typomorph typo_in, float amplitude_1_in, float amplitude_2_in, float rotate_num_in, float rotate_speed_in){
    super(typo_in);
    //    last_update = millis();    
    amplitude_1 = amplitude_1_in;
    amplitude_2 = amplitude_2_in;
    rotate_num = rotate_num_in;
    rotate_speed = rotate_speed_in;
  }

  void setWaves(float amplitude_1_in, float amplitude_2_in, float rotate_num_in, float rotate_speed_in){
    amplitude_1 = amplitude_1_in;
    amplitude_2 = amplitude_2_in;
    rotate_num = rotate_num_in;
    rotate_speed = rotate_speed_in;
  }
  
  void setWavesRot(float rotate_num_in, float rotate_speed_in){
    rotate_num = rotate_num_in;
    rotate_speed = rotate_speed_in;
  }
  
  void setInteractive(boolean interactive_or_not, float val_distance, float val_far, float val_close){
    interactive = interactive_or_not;
    interactive_radius = val_distance;
    interactive_close = val_close;
    interactive_far = val_far;
  }

  void update(){
    /*
     * Update Wave effect = rotation around each point
     *
     * handle each path (wavePath) of each children (waveShape)
     */

    float max_length = typo.getMaxLength();

    wave_ext = new RShape();
    wave_int = new RShape();
    
    for (int i_child = 0; i_child < sampledShape.countChildren(); i_child++){
      waveShape(sampledShape.children[i_child], max_length);
    }
  }
  
  void waveShape(RShape shape, float max_length){
    /*
     * Create wave around the shape
     */
    RShape[] shapes = new RShape[2];
    shapes[0] = new RShape();
    shapes[1] = new RShape();

    float total_length = shape.getCurveLength();
    
    for (int i_p = 0; i_p < shape.countPaths(); i_p ++){
      RPath[] paths = new RPath[2];
      paths = wavePath(shape.paths[i_p], total_length, max_length);

      shapes[0].addPath(paths[0]);
      shapes[1].addPath(paths[1]);
      
      wave_int.addChild(shapes[0]);
      wave_ext.addChild(shapes[1]);
    }
  }

  RPath[] wavePath(RPath path, float total_length, float max_length){
    /*
     * Create a wave around a path
     */

    RPath[] paths = new RPath[2];

    float l = path.getCurveLength();

    // number of rotation depends of the ratio between the length of the path
    // and the longest contour
    int n_rot = floor(max_length / l);

    // init new paths
    RPath path_int = new RPath();
    RPath path_ext = new RPath();

    RPoint[] points = path.getPoints();

    int n_points_loc = points.length;

    // animated angle
    float angle = millis() * rotate_speed / 1000.0;

    for (int i = 0; i < points.length - 1; i++){    
      RPoint pt = points[i];
      RPoint p1 = new RPoint(pt); // copy the point
      RPoint p2 = new RPoint(pt); // copy the point
      
      // init transform matrix
      RMatrix transform = new RMatrix();

      // handle rotation

      // angle that changes along the path
      float loc_angle = i*TWO_PI*int(rotate_num/n_rot)/(n_points_loc-1); 

      // rotate around point
      transform.rotate(angle + loc_angle, pt);
      
      RMatrix t_p1 = new RMatrix(transform);
      RMatrix t_p2 = new RMatrix(transform);
      
      // handle translation
      float loc_amplitude_1 = amplitude_1;
      float loc_amplitude_2 = amplitude_2;
      
      // mouse interactive mode
      if (interactive){
        float d = pt.dist(new RPoint(mouseX, mouseY)); // distance between mouse and current point
        float ratio = map(d, 0, interactive_radius, interactive_far, interactive_close);
        ratio = min(max(0, ratio), 1);
        loc_amplitude_1 *= ratio;
        loc_amplitude_2 *= ratio;
      }

      t_p1.translate(loc_amplitude_1, 0);
      t_p2.translate(loc_amplitude_2, 0);

      // apply transformation
      p1.transform(t_p1);
      p2.transform(t_p2);

      if (i == 0){
        // init paths with the first point
        path_ext = new RPath(p1);
        path_int = new RPath(p2);
      }else{
        // add point to paths
        path_ext.addLineTo(p1);
        path_int.addLineTo(p2);
      }
    }
    
    // close paths
    path_int.addClose();
    path_ext.addClose();

    paths[0] = path_int;
    paths[1] = path_ext;

    // return interior path and exterior path as a list
    return paths;
  }

  void drawExt(){
    /*
     * Draw the external contour
     */
    wave_ext.draw();
  }

  void drawInt(){
    /*
     * Draw the internal contour
     */
    wave_int.draw();
  }
  
  void drawLines(){
    /*
     * Draw lines between contours
     */
    RPoint[] pts1 = wave_ext.getPoints();
    RPoint[] pts2 = wave_int.getPoints();
 
    for (int i = 0; i < pts1.length; i++){
      line(pts1[i].x, pts1[i].y, pts2[i].x, pts2[i].y);
    }
  }

  void draw(){
    drawExt();
    drawInt();
    if (draw_lines){
      drawLines();
    }
  }

  void drawWavesBezier(){
    /*
     * DEPRECATED
     * 
     * Draw bezier curve between all points and a specific point
     */
    RPoint[] pts_ext = wave_ext.getPoints();
    RPoint[] pts_int = wave_int.getPoints();

    RPoint p1 = new RPoint(2000, 1080/2);
    RPoint p0 = new RPoint(1500, 1080/2);

    RShape out =  new RShape();

    for (int i = 0; i < pts_ext.length; i++){
      RPath n = new RPath(new RPoint(pts_int[i]));
      n.addBezierTo(pts_ext[i], p0, p1);
      n.draw();
    }
  }

}

