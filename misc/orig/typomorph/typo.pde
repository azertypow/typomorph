class SortByLength implements Comparator<RPath>{
  public int compare(RPath a, RPath b){
    if (a.getCurveLength() > b.getCurveLength()){
      return -1;
    }else{
      return 1;
    }
  }
}

class Typomorph{
  String txt;
  int text_size;
  
  RShape origShape;             // save original shape
  RShape sampledShape;          // 
  
  int samples;
  
  Typomorph (String txt, String font, int text_size, RPoint pos){
    txt = txt;
    text_size = text_size;
    origShape = RG.getText(txt, font, text_size, CENTER);
    
    RPoint origShape_center = origShape.getCenter();
    
    // center the shape on point pos
    float t_x = pos.x - origShape_center.x / 2.0;
    float t_y = pos.y - origShape_center.y;
    origShape.translate(t_x, t_y);
  }

  Typomorph(RShape shp, RPoint pos){

    origShape = shp;
    
    RPoint origShape_center = origShape.getCenter();
    
    // center the shape on point pos
    float t_x = pos.x - origShape_center.x / 2.0;
    float t_y = pos.y - origShape_center.y;
    origShape.translate(0, 0);
  }
  
  Typomorph (String txt, String font, int text_size, float pos_x, float pos_y){
    this(txt, font, text_size, new RPoint(pos_x, pos_y));
  }

  Typomorph (String txt, String font, int text_size, RPoint pos, int anchor){
    txt = txt;
    text_size = text_size;
    origShape = RG.getText(txt, font, text_size, anchor);
    
    RPoint origShape_center = origShape.getCenter();
    
    // center the shape on point pos
    float t_x = pos.x;
    float t_y = pos.y;
    origShape.translate(t_x, t_y);
  }
  
  RShape getShape(){
    /*
     * Return the sampledShape if it exists, otherwise return original shape 
     */
    if (sampledShape == null){
      return origShape;
    }else{
      return sampledShape;      
    }
  }

  void draw(){
    /*
     * Draw the sampledShape if it exists, otherwise return original shape 
     */
    if (sampledShape == null){
      origShape.draw();
    }else{
      sampledShape.draw();      
    }
  }

  RShape resampleByPath(int n_samples_in){
    sampledShape = new RShape();

    for (int i_c = 0; i_c < origShape.countChildren(); i_c ++){
      RShape shape_loc = new RShape();
   
      for (int i_p = 0; i_p < origShape.children[i_c].countPaths(); i_p ++){      
        // ratio of each segments relative to total length
        float frac = 1.0/n_samples_in;

        // init a list of points
        RPoint[] cur_points = new RPoint[n_samples_in];
        
        for (int ii = 0; ii < cur_points.length; ii ++){
          // get each point
          cur_points[ii] = origShape.children[i_c].paths[i_p].getPoint(frac * ii);
        }
        
        // create path from point
        RPath path_loc = new RPath(cur_points);
        
        // add path to shape
        shape_loc.addPath(path_loc);
      }
      // add shape to main shape as children
      sampledShape.addChild(shape_loc);
    }
    return sampledShape; 
  }

  RShape sortByLength(){
    RShape sortedShape = new RShape();

    for (int i_c = 0; i_c < sampledShape.countChildren(); i_c++){
      RPath[] paths = sampledShape.children[i_c].paths;
      Arrays.sort(paths, new SortByLength());
      RShape shape_loc = new RShape(paths);
      sortedShape.addChild(shape_loc);
    }

    sampledShape = sortedShape;
    return sampledShape; 

  }

  RShape resample(int n_samples_in, boolean samples_by_letter){
    /*
     * Resample the original shape and return sampledShape
     * the shape is transformed into -n_sample_in- straight segment distributed into each paths
     *
     * if sample_by_letter == true, each letter has -n_sample_in- segments
     */
    samples = n_samples_in;
    
    int loc_n_samples = n_samples_in;
    if (!samples_by_letter){
      loc_n_samples = samples/origShape.countChildren();
    }
    
    sampledShape = new RShape();

    for (int i_c = 0; i_c < origShape.countChildren(); i_c ++){
      RShape shape_loc = new RShape();
      float total_length = origShape.children[i_c].getCurveLength();
      
      for (int i_p = 0; i_p < origShape.children[i_c].countPaths(); i_p ++){
        float l =  origShape.children[i_c].paths[i_p].getCurveLength();
        
        // calculate the number of segment related to relative lengths
        int n_points_loc = int(map(l, 0, total_length, 0, loc_n_samples));

        // the shape cannot have less than 2 points
        n_points_loc = max(n_points_loc, 2);

        // ratio of each segments relative to total length
        float frac = 1.0/n_points_loc;

        // init a list of points
        RPoint[] cur_points = new RPoint[n_points_loc];
        
        for (int ii = 0; ii < cur_points.length; ii ++){
          // get each point
          cur_points[ii] = origShape.children[i_c].paths[i_p].getPoint(frac * ii);
        }
        
        // create path from point
        RPath path_loc = new RPath(cur_points);

        // add path to shape
        shape_loc.addPath(path_loc);
      }
      // add shape to main shape as children
      sampledShape.addChild(shape_loc);
    }
    return sampledShape; 
  }

  float getMaxLength(){
    float max_length = 0.0;
    for (int i_child = 0; i_child < sampledShape.countChildren(); i_child++){
      for (int i_path = 0; i_path < sampledShape.children[i_child].countPaths(); i_path++){
        max_length = max(max_length, sampledShape.children[i_child].paths[i_path].getCurveLength());
      }
    }
    return max_length;
  }
}
