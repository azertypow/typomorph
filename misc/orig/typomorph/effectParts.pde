class EffectPart extends Effect{  
  boolean draw_lines = false;
  float amp = 0;
  RShape shape_norm;
  
  ArrayList<RPoint[]> lines;
  ArrayList<Part> parts;

  EffectPart (Typomorph typo_in){
    super(typo_in);
    parts = new ArrayList<Part>();
  }
  
  EffectPart (Typomorph typo_in, float amp_in){
    this(typo_in);
    amp = amp_in;
    getNormals();
  }

  void getNormals(){
    shape_norm = new RShape();

    lines = new ArrayList<RPoint[]>();
    
    for (int i_child = 0; i_child < sampledShape.countChildren(); i_child++){
      RShape child = handleShape(sampledShape.children[i_child]);
      shape_norm.addChild(child);
    }
  }

  void updateTypo(Typomorph typo_in){
    typo = typo_in;
    sampledShape = typo.getShape();
    getNormals();
  }
  
  void update(){
    ArrayList<Part> toerase = new ArrayList<Part>();

    for(Part p : parts){
      if (p.is_alive()){
        p.update();
      }else{
        toerase.add(p);
      } 
    }

    for(Part p : toerase){
      parts.remove(p);
    }
  }
  
  void draw(){
    for(Part p : parts){
      p.draw();
    }
  }

  void addParts(int type_p){
    for (RPoint[] l : lines){
      //line(l[0].x, l[0].y, l[1].x, l[1].y);
      parts.add(new Part(l, int(random(100, 1000)), type_p));
    }
  }

  void addPartsRand(int num){
    for (int i=0; i<num;i++){
      int n = int(random(0, lines.size()));
      parts.add(new Part(lines.get(n), int(random(200,2000)), 1));
    }
  }
  
  void addPartsAtDist(float x, float d, int num){
    
    for (int i=0; i < num;i++){
      RPoint[] l = lines.get(int(random(0, lines.size())));
      if (abs(l[0].x - x) < d) {
        parts.add(new Part(l, int(random(100, 2000)), 2));
      }
    }
  }
  
  RShape getShape(){
    return shape_norm;
  }

  void setAmplitude(float amp_in){
    amp = amp_in;
  }
  
  void setDrawLines(boolean draw_lines_in){
    draw_lines = draw_lines_in;
  }

  ArrayList<RPoint[]> getLines(){
    return lines; 
  }

  RShape handleShape(RShape shape_in){
    RShape out = new RShape();
    for (int i_p = 0; i_p < shape_in.countPaths(); i_p ++){
      out.addPath(handlePath(shape_in.paths[i_p]));
    }
    return out;
  } 
  
  RPath handlePath(RPath path){
    RPoint[] points = path.getPoints();
    RPath cont = new RPath();
    
    for (int i = 0; i < points.length; i++){
      int i_p = i-1;
      
      if (i_p < 0){
        i_p = points.length - 1;
      }

      int i_n = i + 1;
      
      if (i_n >= points.length){
        i_n = 0;
      }
      
      RPoint p = new RPoint(points[i]);
      RPoint p_p = new RPoint(points[i_p]);
      RPoint p_n = new RPoint(points[i_n]);
      
      p_n.sub(p_p);
      p_n.normalize();
      p_n.rotate(-HALF_PI);      
      p_n.scale(amp);
      
      RPoint nn = new RPoint(p);
      nn.add(p_n);
      
      if (draw_lines){
        line(p.x, p.y, nn.x, nn.y);
      }
      
      RPoint[] new_line = new RPoint[2];

      new_line[0] = new RPoint(p.x, p.y);
      new_line[1] = new RPoint(nn.x, nn.y);
      
      lines.add(new_line);

      if (i == 0){
        cont = new RPath(nn);
      }else{
        cont.addLineTo(nn);
      }
    }
    cont.addClose();
    return cont;
  }
}
