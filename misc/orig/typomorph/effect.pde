class Effect{

  Typomorph typo;
  RShape sampledShape;

  Effect (Typomorph typo_in){
    typo = typo_in;
    sampledShape = typo.getShape();
  }

  void update(){

  }

  void draw(){

  }
}
