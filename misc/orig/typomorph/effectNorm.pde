class EffectNorm extends Effect{  
  boolean draw_lines = false;
  float amp = 0;
  RShape shape_norm;
  
  EffectNorm (Typomorph typo_in){
    super(typo_in);
  }
  
  EffectNorm (Typomorph typo_in, float amp_in){
    super(typo_in);
    amp = amp_in;
  }

  void update(){
    shape_norm = new RShape();
    for (int i_child = 0; i_child < sampledShape.countChildren(); i_child++){
      RShape child = handleShape(sampledShape.children[i_child]);
      shape_norm.addChild(child);
    }
  }
  
  void draw(){
    shape_norm.draw();
  }
  
  RShape getShape(){
    return shape_norm;
  }

  void setAmplitude(float amp_in){
    amp = amp_in;
  }
  
  void setDrawLines(boolean draw_lines_in){
    draw_lines = draw_lines_in;
  }

  RShape handleShape(RShape shape_in){
    RShape out = new RShape();
    for (int i_p = 0; i_p < shape_in.countPaths(); i_p ++){
      out.addPath(handlePath(shape_in.paths[i_p]));
    }
    return out;
  } 
  
  RPath handlePath(RPath path){
    RPoint[] points = path.getPoints();
    RPath cont = new RPath();
    
    for (int i = 0; i < points.length; i++){
      int i_p = i-1;
      
      if (i_p < 0){
        i_p = points.length - 1;
      }

      int i_n = i + 1;
      
      if (i_n >= points.length){
        i_n = 0;
      }
      
      RPoint p = new RPoint(points[i]);
      RPoint p_p = new RPoint(points[i_p]);
      RPoint p_n = new RPoint(points[i_n]);
      
      p_n.sub(p_p);
      p_n.normalize();
      p_n.rotate(-HALF_PI);      
      p_n.scale(amp);
      
      RPoint nn = new RPoint(p);
      nn.add(p_n);
      
      if (draw_lines){
        line(p.x, p.y, nn.x, nn.y);
      }
      
      if (i == 0){
        cont = new RPath(nn);
      }else{
        cont.addLineTo(nn);
      }
    }
    cont.addClose();
    return cont;
  }
}
