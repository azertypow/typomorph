class MorphWaves{
  EffectWaves wavesA;
  EffectWaves wavesB;

  RPoint[] pointsA;
  RPoint[] pointsB;
  
  RPoint[] points;
  RPoint[] points_lines;
  
  int n_samples;

  MorphWaves (EffectWaves wavesA_in, EffectWaves wavesB_in){
    wavesA = wavesA_in;
    wavesB = wavesB_in;
    
    pointsA = wavesA.typo.getShape().getPoints();
    pointsB = wavesB.typo.getShape().getPoints();
    
    n_samples = min(pointsA.length, pointsB.length);
  }

  MorphWaves (EffectWaves wavesA_in, EffectWaves wavesB_in, int n_samples_in){
    wavesA = wavesA_in;
    wavesB = wavesB_in;
    
    pointsA = wavesA.typo.getShape().getPoints();
    pointsB = wavesB.typo.getShape().getPoints();

    n_samples = n_samples_in;
  }

  
  RPoint[] getPoints(float morph){
    int min_points = min(pointsA.length, pointsB.length);
    RPoint[] pts_out = new RPoint[min_points];
    for (int i = 0; i < min_points; i++){
      float pos_x = lerp(pointsA[i].x, pointsB[i].x, morph);
      float pos_y = lerp(pointsA[i].y, pointsB[i].y, morph);
      pts_out[i] = new RPoint(pos_x, pos_y);
    }
    
    return pts_out;
  }
  
  void drawPoints(float morph){
    points = getPoints(morph);
    for (int i = 0; i < points.length; i++){
      point(points[i].x, points[i].y);
    }
  }
  
  void drawLines(float morph1, float morph2){
    points = getPoints(morph1);
    points_lines = getPoints(morph2);
    
    for (int i = 0; i < points.length; i++){
      line(points[i].x, points[i].y, points_lines[i].x, points_lines[i].y);
    }
  }
  
  void bezierMorph(float morph_p, float morph_w){
    wavesA.update();
    wavesB.update();
    
    RPoint[] ptsA_int = wavesA.wave_int.getPoints();
    RPoint[] ptsA_ext = wavesA.wave_ext.getPoints();
    RPoint[] ptsB_int = wavesB.wave_int.getPoints();
    RPoint[] ptsB_ext = wavesB.wave_ext.getPoints();
    
    float morph1 = max(min(morph_p+morph_w/2, 1), 0);
    float morph2 = min(max(morph_p-morph_w/2, 0), 1);
    
    for (int i = 0; i < n_samples; i++){
      RPath p = new RPath(ptsA_int[i]);
      p.addBezierTo(ptsA_ext[i], ptsB_ext[i], ptsB_int[i]);
      RPath p2 = p.split(morph1)[0];
      
      if (morph1 > 0.01 && morph1 > morph2){
        p2 = p2.split(morph2/morph1)[1];      
      }

      p2.draw();
    }
  }
}
