/*
 * Typomorph version 0.2
 *
 * Author: Laurent Malys (lo@crans.org)
 */

boolean export = false;
float n_steps = 60.0;

boolean b_save = false;

import peasy.*;

import geomerative.*;
import java.util.*;

import oscP5.*;
import netP5.*;

import controlP5.*;

float framerate_gui = 0;

int num_ecran = 0;

//String font_file ="LiberationSans-Regular.ttf";
String font_file ="Roboto-Bold.ttf";
//String font_file ="DejaVuSans.ttf";
//String font_file ="UniversNextArabic-Regular.ttf";
//String font_file ="JKG-M_3.ttf";

PeasyCam cam;

// float cam_x = 0.42;
// float cam_y = 0.36;

float cam_x = 0;
float cam_y = 0;

boolean mode_gui = true;
boolean mode_3d = false;
boolean mode_shape = true;  // show the outline
boolean mode_lines = false; // show the lines 
boolean mode_sym = true;    // 
boolean mode_mouse = false; // effect intensity according to mouse distance
boolean mode_anim = true;

float t = 0.0;

int suite_step = 1;

// String[] suite_letters = {"A", "B", "C", "D", "E", "F",
//                           "G", "H", "I", "J", "K", "L",
//                           "M", "N", "O", "P", "Q", "R","S", "T", "U", "V", "W" , "X", "Y", "Z"};

// String[] suite_letters = {"T", "Y", "P", "O", "M", "O", "R", "P", "H"};
String[] suite_letters = {"A", "B", "C", "D", "E"};

String[] wordToLetters(String word){
  String[] letters = new String[word.length()];
  for (int i =0; i< word.length();i++){
    String s = new String();
    letters[i] = s.valueOf(word.charAt(i));
  }
    return letters;
}

String pickKanji(){
  int start = Integer.parseInt("4e00", 16);
  int end = Integer.parseInt("9faf", 16);
  int pick = 0;
  RPoint[] pts = null;
  while(pts == null)
  { 
    pick = int(random(start, end));
    println(String.valueOf(Character.toChars( pick )));
    
    Typomorph typo = new Typomorph(String.valueOf(Character.toChars( pick )), font_file, 100, new RPoint(0,0));
    RShape sh = typo.getShape();
    pts = sh.getPoints();
    if ( pts != null )
    {
      println(pts.length);
      if ( pts.length == 6 ){
        pts = null;
      }
    }
    //  sh.getPoints().length;
  }
  
  return String.valueOf(Character.toChars( pick ));
}

int[] suite_zoom = {1, 3, 5, 7, 9, 7, 5, 3};
int pos_zoom = -1;
boolean change_zoom = false;
float cur_zoom = 9./1.;

int suite_letters_length = 2;

//String[] suite_letters = new String[suite_letters_length];

//String[] suite_letters = wordToLetters("こんにちは世界");
// String[] suite_letters = {String.valueOf('\u4e07')};

// String[] suite_letters =  {"أ",
//                              "ب",
//                              "ج",
//                              "د",
//                              "ه",
//                              "و",
//                              "ز",
//                              "ح",
//                               "ط",
//                              "ي",
//                               "ك",
//                              "ل",
//                              "م",
//                              "ن",
//                              "س",
//                               "ع",
//                              "ف",
//                              "ص",
//                              "ق",
//                               "ر",
//                              "ش",
//                               "ت",
//                              "ث",
//                              "خ",
//                              "ذ",
//                               "ض",
//                              "ظ",
//                              "غ"};

int suite_step_max = suite_letters.length;
ControlP5 cp5;
RadioButton r1;
Slider fr_slider;
OscP5 oscP5;

float seg_length_1 = 1000;
float seg_length_2 = 0;
int n_samples = 100;

float morph = 0.0;

float rotate_speed = 0.001;
float rotate_amoung = 1.0;

float mouse_radius = 400;
float mouse_min = 1;
float mouse_max = -1;

int text_size = 500;

float mode = 1.0;

String input, inputB;

Typomorph typo;
Morph morph_test;

EffectPart parts;
EffectPart parts2;

EffectSpiral spiral;
float vel_spiral = 20;

int cpt = 0;

ArrayList<PVector> pts; 
int cpt_pts;

float ease(float p) {
  return 3*p*p - 2*p*p*p;
}

float ease(float p, float g) {
  if (p < 0.5) 
    return 0.5 * pow(2*p, g);
  else
    return 1 - 0.5 * pow(2*(1 - p), g);
}

float ease_ar(float p, float g){
  if (p<0.5){
    return ease(p*2, g);
  }else{
    return ease((1-p)*2, g);
  }
}

void setup(){
  // Initilaize the sketch
  size(1080,1080, P3D);
  //fullScreen(P3D, 2);

  cam = new PeasyCam(this, 625);
  cam.setMinimumDistance(50);
  cam.setMaximumDistance(5000);

  pts = new ArrayList<PVector>();
  cpt_pts = 0;

  //  ortho();
  
  if (!mode_3d){
    cam.setActive(false);
  }

  colorMode(RGB, 255);

  text_size = 500;
  n_samples = 400;
  input = "8";
  inputB = "Bye!";

  setupGui(); 
  oscP5 = new OscP5(this,9000);

  // Choice of colors
  background(0);
  noFill();
  stroke(255);

  // VERY IMPORTANT: Allways initialize the library in the setup
  RG.setPolygonizer(RG.UNIFORMSTEP);
  RG.setPolygonizerStep(1);
  RG.init(this);

  strokeWeight(1.2);


  typo = new Typomorph(input, font_file, text_size, new RPoint(0, 0));
  typo.resample(n_samples, false);
  
  parts = new EffectPart(typo, seg_length_1);
  parts2 = new EffectPart(typo, seg_length_1);

  spiral = new EffectSpiral(typo);
  
  // for (int i = 0; i < suite_letters_length; i++){
  //   suite_letters[i] = pickKanji();
  // }

  // Enable smoothing
  smooth(10);
}



void setupGui(){
  cp5 = new ControlP5(this);
  cp5.setAutoDraw(false);
  int cp5_y = 0;

  cp5_y += 10;
  cp5.addTextfield("input").setPosition(10,cp5_y).setSize(200,15).setFocus(true).setValue(input);
  cp5_y += 15;

  cp5_y += 20;
  cp5.addSlider("text_size").setPosition(10,cp5_y).setSize(200, 15).setRange(70, 1200).setValue(500);

  cp5_y += 20;

  r1 = cp5.addRadioButton("mode")
    .setPosition(10,cp5_y)
    .setSize(30,15)
    .setItemsPerRow(6)
    .setSpacingColumn(50)
    .addItem("Waves", 1)
    .addItem("Deform", 2)
    .addItem("Morph", 3)
    .addItem("Suite", 4)
    .addItem("Parts", 5)
    .addItem("Cur", 6)
    .setValue(1);

  cp5_y += 20;
  cp5.addSlider("rotate_amoung").setPosition(10,cp5_y).setSize(200, 15).setRange(0, 5).setValue(3);

  cp5_y += 20;
  cp5.addSlider("rotate_speed").setPosition(10,cp5_y).setSize(200, 15).setRange(0.0, 10).setValue(4);

  cp5_y += 20;
  cp5.addSlider("n_samples").setPosition(10,cp5_y).setSize(200, 15).setRange(4, 1000).setValue(500);

  cp5_y += 20;
  cp5.addSlider("seg_length_1").setPosition(10,cp5_y).setSize(200, 15).setRange(0, 1000).setValue(500);

  cp5_y += 20;
  cp5.addSlider("seg_length_2").setPosition(10,cp5_y).setSize(200, 15).setRange(-500, 500).setValue(0);

  cp5_y += 20;
  cp5.addSlider("morph").setPosition(10,cp5_y).setSize(200, 15).setRange(0.0, 1.0);

  cp5_y += 20;
  cp5.addSlider("vel_spiral").setPosition(10,cp5_y).setSize(200, 15).setRange(0, 40);
  
  cp5_y += 20;
  cp5.addToggle("mode_shape").setPosition(10,cp5_y).setSize(30,15);
  cp5.addToggle("mode_lines").setPosition(100,cp5_y).setSize(30,15);
  
  cp5_y += 35;
  cp5.addToggle("mode_mouse").setPosition(10,cp5_y).setSize(30,15);
  cp5.addToggle("mode_3d").setPosition(100,cp5_y).setSize(30,15);

  cp5_y += 35;
  cp5.addToggle("mode_anim").setPosition(10,cp5_y).setSize(30,15);

  cp5_y += 35;
  cp5.addSlider("cam_x").setPosition(10,cp5_y).setSize(200, 15).setRange(-3.0, 3.0);

  cp5_y += 20;
  cp5.addSlider("cam_y").setPosition(10,cp5_y).setSize(200, 15).setRange(-3.0, 3.0);

  cp5_y += 20;
  fr_slider = cp5.addSlider("framerate_gui").setPosition(10,cp5_y).setSize(200, 15).setRange(0, 100);
  

}


void gui() {
  hint(DISABLE_DEPTH_TEST);
  cam.beginHUD();  
  cam.setActive(mode_3d);
  if (mode_gui){
    cp5.draw();
  }
  cam.endHUD();
  hint(ENABLE_DEPTH_TEST);
}


Morph initMorph(String i1, String i2, int x1, int x2){
  Typomorph typoA = new Typomorph(i1, font_file, text_size, new RPoint(x1,x2));
  Typomorph typoB = new Typomorph(i2, font_file, text_size, new RPoint(x1,x2));

  // typoA.resample(n_samples, false);
  // typoB.resample(n_samples, false);

  typoA.resampleByPath(n_samples);
  typoB.resampleByPath(n_samples);

  typoA.sortByLength();
  typoB.sortByLength();


  return new Morph(typoA, typoB);
}

MorphWaves initMorphWaves(String i1, String i2, int x1, int x2){
  Typomorph typoA = new Typomorph(i1, font_file, text_size, new RPoint(x1,0));
  Typomorph typoB = new Typomorph(i2, font_file, text_size, new RPoint(x2,0));
  
  typoA.resample(n_samples, false);
  typoB.resample(n_samples, false);

  EffectWaves wavesA = new EffectWaves(typoA, seg_length_1, seg_length_2, rotate_amoung, rotate_speed);
  EffectWaves wavesB = new EffectWaves(typoB, seg_length_1, seg_length_2, rotate_amoung, rotate_speed);
  
  return new MorphWaves(wavesA, wavesB);
}

MorphWaves initMorphWavesEcran(String i1, String i2, RPoint pt1, RPoint pt2){
  Typomorph typoA = new Typomorph(i1, font_file, text_size, pt1);
  Typomorph typoB = new Typomorph(i2, font_file, text_size, pt2);
  
  typoA.resample(n_samples, false);
  typoB.resample(n_samples, false);
  
  EffectWaves wavesA = new EffectWaves(typoA, seg_length_1, seg_length_2, rotate_amoung, rotate_speed);
  EffectWaves wavesB = new EffectWaves(typoB, seg_length_1, seg_length_2, rotate_amoung, rotate_speed);
  
  return new MorphWaves(wavesA, wavesB);
}


void draw(){

  framerate_gui = frameRate;
  fr_slider.setValue(framerate_gui);

 
  float cam_xx = millis()/1000.0;
  
  if (export){
    cam_xx = frameCount/40.0;
  }
  
  cam.setRotations(cam_y, cam_x, 0);
  
  background(0);
  
  //  rect(-width/2, -height/2, width, height);

  if (mode_anim){
    morph = ease((t%int(n_steps))/n_steps, 1);
    //morph = (t%20)/20.0;
  }

  if (mode==1.0){
    Typomorph typo_test = new Typomorph(input, font_file, text_size, new RPoint(0, 0));
    typo_test.resample(n_samples, false);
    EffectWaves wave_test = new EffectWaves(typo_test, seg_length_1, seg_length_2, rotate_amoung, rotate_speed);
    wave_test.setInteractive(mode_mouse, 400, 1, 0);
    wave_test.update();
    if (mode_lines){
      wave_test.drawLines();
    }
    if(mode_shape){
      wave_test.drawInt();
      wave_test.drawExt();
    }
  }else if (mode==2.0){
    Typomorph typo_test = new Typomorph(input, font_file, text_size, new RPoint(0, 0));
    typo_test.resample(n_samples, false);
    EffectNorm norm_test = new EffectNorm(typo_test);
    norm_test.setAmplitude(seg_length_1);
    norm_test.setDrawLines(mode_lines);
    norm_test.update();
    norm_test.draw();
 
 }else if (mode==3.0){
    morph_test = initMorph(input, inputB, 0, 0);
    if (mode_lines){
      morph_test.drawLines(0, morph);
    }else{
      morph_test.morphByPath(morph).draw();   
    }
    
  }else if (mode==4.0){
    if (t%40 == 0){
      suite_step += 1;
      if (suite_step == suite_step_max){
        suite_step = 0;
      }
    }
    
    int n = suite_step;
    int n1 = (suite_step+1)%suite_step_max;

    MorphWaves morph2 = initMorphWaves(suite_letters[n], suite_letters[n1], 0-int(morph*1000), 1000-int(morph*1000));
    morph2.bezierMorph(map(morph, 0, 1, -0.2, 1.2), 0.41);
  }else if (mode==5.0){
      parts.addPartsRand(5);
      parts.addPartsAtDist(mouseX, seg_length_2, 400);
      parts.update();
      parts.draw();
  }else if (mode==6.0){
    draw_cur_image();
  }

  if (mode_anim){
    t += 1.0;
    if (t%(int(n_steps)) == 0){
      cpt += 1;
      if (cpt%4 == 0){
        change_zoom = true;
        pos_zoom = (pos_zoom+1) % suite_zoom.length;
      }else{
        change_zoom = false;
      }
    }
  }

  if (export){
    saveFrame();
    // if (t >= 10*40.0){
    //   noLoop();
    // }
    if (cpt == suite_letters.length*2){
      noLoop();
    }
  }

  if (b_save){
    saveFrame();
    b_save = false;
  }
  gui();
}

void draw_cur_image(){
  int prec_x = 0;
  int prec_y = 0;
  int text_size_rel = int(0.8 * text_size);
  for (int i = 0; i < suite_letters.length; i++){
    int top =  int(-2.5 * text_size_rel) + i * text_size_rel;
    int pos_x = int(-text_size_rel*0.9
                    );
    if (i > 4){
      pos_x = int(text_size_rel*0.9
                  );
      top = - 3 * text_size_rel + (i-3)*text_size_rel;
    }
    if (i > 0){

      Typomorph typoA = new Typomorph(suite_letters[i-1], font_file, text_size, new RPoint(prec_x, prec_y));
      Typomorph typoB = new Typomorph(suite_letters[i], font_file, text_size, new RPoint(pos_x,top));
  
      typoA.resample(n_samples, false);
      typoB.resample(n_samples, false);

      EffectWaves wavesA = new EffectWaves(typoA, seg_length_1, seg_length_2, rotate_amoung, rotate_speed);
      EffectWaves wavesB = new EffectWaves(typoB, seg_length_1, seg_length_2, rotate_amoung, rotate_speed);
  
      MorphWaves mor = new MorphWaves(wavesA, wavesB);

      noFill();
      strokeWeight(1);
      stroke(255, 100);
      mor.bezierMorph(map(morph, 0, 1, -0.2, 1.2), 1);
    }
    prec_x = pos_x;
    prec_y = top;
  }

  for (int i = 0; i < suite_letters.length; i++){
    int top =  int(-2.5 * text_size_rel) + i * text_size_rel;
    int pos_x = -int(text_size_rel*0.9);
    if (i > 4){
      pos_x = int(text_size_rel*0.9);
      top = - 3 * text_size_rel + (i-3)*text_size_rel;
    }
    Typomorph let = new Typomorph(suite_letters[i], font_file, text_size, new RPoint(pos_x, top));
    //  fill(255, 255);
    strokeWeight(4);
    stroke(0, 255);
    let.draw();
   }
}

void draw_cur_kanji_morphing(){
  fill(255);
  // noStroke();
  noFill();
  if (change_zoom){
    cur_zoom = map(morph, 0, 1, 9./(suite_zoom[pos_zoom]), 9./(suite_zoom[(pos_zoom+1)%suite_zoom.length]));
  }
  scale(cur_zoom);
  //strokeWeight(1.2/cur_zoom);
  int n_x = 2;
  int delta_x = width/(n_x);

  int n_y = n_x;
  int delta_y = width/(n_y);

  int hh = (delta_x - 10);

  for (int j = 0; j < n_y; j++){
    float y = -height/2 + delta_y/2.0 + j*delta_y;
    for (int i = 0; i < n_x; i++){
      int pos = i + j*n_x;
      int n = (cpt + pos*81)%suite_letters.length;
      int n1 = (cpt + 1 + pos*81)%suite_letters.length;
      float x = -width/2 + delta_x/2.0 + i*delta_x;
      Morph m1 = initMorph(suite_letters[n], suite_letters[n1], int(x), int(y));
      m1.morphByPath(morph).draw();
      // rect(int(x)-hh/2, int(y)-hh/2, hh, hh);
      //      cpt += 1;
    }
  }
}


void draw_cur__(){
  int n_x = 1;
  int delta_x = width/(n_x);

  int n_y = 1;
  int delta_y = width/(n_y);

  
  for (int j = 0; j < n_y; j++){
    float y = -height/2 + delta_y/2 + j*delta_y;
    for (int i = n_x-1; i >= 0; i--){
      int n = (cpt + i + j*n_x)%suite_letters.length;
      int n1 = (cpt + 1 + i + j*n_x)%suite_letters.length;
      float x = -width/2 + delta_x/2.0 + i*delta_x;
      Morph m1 = initMorph(suite_letters[n], suite_letters[n1], int(-x), int(y));
      RShape sh = m1.morphByPath(morph);
      Typomorph ty = new Typomorph(sh, new RPoint(0,0));

      stroke(255);
      ty.draw();
      parts.updateTypo(ty);
      parts.addParts(1);

      parts.update();
      parts.draw();
      
      if (t%int(n_steps) == 0){
        parts2.updateTypo(ty);
        parts2.addParts(0);
        //        parts2.addParts(1);
        //        parts.addParts(0);
      }
      
      parts2.update();
      parts2.draw();

      fill(0, 0, 0, 255);
      //noStroke();
      //      ty.getShape().draw();
      noFill();
      //      cpt += 1;
    }
  }

}

void keyPressed(){
  if (key == 'r'){
    b_save = true;
  }
}

void draw_cur(){
  int n_x = 1;
  int delta_x = width/(n_x);

  int n_y = 1;
  int delta_y = width/(n_y);
  
  for (int j = 0; j < n_y; j++){
    float y = -height/2 + delta_y/2 + j*delta_y;
    for (int i = n_x-1; i >= 0; i--){
      int n = (cpt + i + j*n_x)%suite_letters.length;
      int n1 = (cpt + 1 + i + j*n_x)%suite_letters.length;
      float x = -width/2 + delta_x/2.0 + i*delta_x;
      Morph m1 = initMorph(suite_letters[n], suite_letters[n1], int(-x), int(y));
      RShape sh = m1.morphByPath(morph);
      Typomorph ty = new Typomorph(sh, new RPoint(0,0));

      stroke(255);
      spiral.updateTypo(ty);
      //      spiral.update(2+ease_ar((t%int(n_steps))/n_steps, 1)*vel_spiral);
      spiral.update(vel_spiral);
      spiral.draw();
      
    }
  }
}


void input(String i){
  typo = new Typomorph(i, font_file, text_size, new RPoint(0,0));
  typo.resample(n_samples, false);
  parts = new EffectPart(typo, seg_length_1);
  input = i;
  
}

void controlEvent(ControlEvent theEvent) {
  if(theEvent.isFrom(r1)) {
    print("got an event from "+theEvent.getName()+"\t");
    mode = theEvent.getValue();
  }
}

/* incoming osc message are forwarded to the oscEvent method. */
void oscEvent(OscMessage theOscMessage) {
  /* print the address pattern and the typetag of the received OscMessage */
  print("### received an osc message.");
  print(" addrpattern: "+theOscMessage.addrPattern());
  println(" typetag: "+theOscMessage.typetag());

  switch(theOscMessage.addrPattern()){


  case "/1/fader1":
    text_size = int(map(theOscMessage.get(0).floatValue(), 0, 1.0, 100, 1000));
    break;
  case "/1/fader2":
    seg_length_1 = int(map(theOscMessage.get(0).floatValue(), 0, 1.0, 2, 1000));
    break;
  case "/1/fader3":
    n_samples = int(map(theOscMessage.get(0).floatValue(), 0, 1.0, 2, 1000));
    break;
    
  case "/midi/[1]cc11":
    n_samples = int(map(theOscMessage.get(0).intValue(), 0, 127, 2, 1000));
    break;
  case "/midi/[1]cc12":
    rotate_speed = map(theOscMessage.get(0).intValue(), 0, 127, 0.0, 0.01);
    break;
  case "/midi/[1]cc13":
    rotate_amoung = map(theOscMessage.get(0).intValue(), 0, 127, 0, 50);
    break;
  case "/midi/[1]cc14":
    seg_length_1 = map(theOscMessage.get(0).intValue(), 0, 127, 0, 500);
    break;
  case "/midi/[1]cc15":
    seg_length_2 = map(theOscMessage.get(0).intValue(), 0, 127, -100, 100);
    break;
  case "/midi/[1]cc16":
    text_size = int(map(theOscMessage.get(0).intValue(), 0, 127, 100, 1000));
    break;
    
  case "/midi/[1]cc17":
    vel_spiral = map(theOscMessage.get(0).intValue(), 0, 127, 0.5, 400.0);
    break;

  case "/midi/[1]cc18":
    cam_y = map(theOscMessage.get(0).intValue(), 0, 127, 0, -HALF_PI);
    break;

  case "/midi/[1]cc21":
    mouse_radius = map(theOscMessage.get(0).intValue(), 0, 127, 0, 1000);
    break;
  case "/midi/[1]cc22":
    mouse_min = map(theOscMessage.get(0).intValue(), 0, 127, -1.0, 1.0);
    break;
  case "/midi/[1]cc23":
    mouse_max = map(theOscMessage.get(0).intValue(), 0, 127, -1.0, 1.0);
    break;
    
    // modes
  case "/midi/[1]cc101":
    mode_gui = (theOscMessage.get(0).intValue() == 0);
    break;
  case "/midi/[1]cc102":
    mode_lines = (theOscMessage.get(0).intValue() == 0);
    break;
  case "/midi/[1]cc103":
    mode_shape = (theOscMessage.get(0).intValue() == 0);
    break;

  }
}
