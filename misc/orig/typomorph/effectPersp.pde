class EffectPersp extends Effect{  
  boolean draw_lines = true;
  EffectPersp (Typomorph typo_in){
    super(typo_in);
  }
  
  void update(){
    for (int i_child = 0; i_child < sampledShape.countChildren(); i_child++){
      handleShape(sampledShape.children[i_child]);
    }
    typo.draw();
  }
  
  void draw(){
    
  }

  void handleShape(RShape shape_in){
    for (int i_p = 0; i_p < shape_in.countPaths(); i_p ++){
      handlePath(shape_in.paths[i_p]);
    }    
  } 
  
  void handlePath(RPath path){
    RPoint[] points = path.getPoints();
    RPath cont = new RPath();
    
    RPoint m = new RPoint(mouseX, mouseY);

    for (int i = 0; i < points.length; i++){     
      RPoint p = new RPoint(points[i]);
      
      RPoint dir = new RPoint(p);
      dir.sub(m);
      dir.normalize();
      dir.scale(map(m.dist(p), 0, 500, 0, 100));

      RPoint nn = new RPoint(p);
      nn.add(dir);

      if (draw_lines){
        line(p.x, p.y, nn.x, nn.y);
      }

      if (i == 0){
        cont = new RPath(nn);
      }else{
        cont.addLineTo(nn);
      }
    }
    cont.addClose();
    cont.draw();
  }
}
