Typomorph
=========

Expérience de morphing de typographie avec Processing et la librairie Geomerative

## Dépendances

Processing avec les librairies suivantes :
- Geomerative
- ControlP5
- oscP5

Plus, pour la 3d : 
- Peasycam

## Documentation 

### Principe

La librairie Geomerative est utilisée pour transformer un texte en dessin vectoriel sous forme d'objets PShape. On peut alors sampler/discrétiser les différents contours pour ajouter certains effets.

### Les objets

#### Typomorph et Effect

L'objet principal de typomorph est l'objet Typomorph qui permet d'afficher du texte en choisissant une fonte.

Une fois un objet Typomorph créé. On peut lui ajouter différents effets :
- Waves
- Norm
- Persp


#### Morph

L'objet Morph permet de créer des animations de morphing entre différentes formes.
