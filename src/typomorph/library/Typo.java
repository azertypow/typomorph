/**
 * Copyright 2019 Laurent Malys
 *
 * This file is part of TypoMorph.
 *
 * TypoMorph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Geomerative is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TypoMorph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package typomorph;

import java.util.*;
import processing.core.*;
import geomerative.*;

class SortByLength implements Comparator<RPath>{
  public int compare(RPath a, RPath b){
    if (a.getCurveLength() > b.getCurveLength()){
      return -1;
    }else{
      return 1;
    }
  }
}

public class Typo {

  // myParent is a reference to the parent sketch
  public PApplet applet;

  int myVariable = 0;

	int text_size = 100;

  RShape origShape;
  RShape sampledShape;

  int samples;

	public final static String VERSION = "##library.prettyVersion##";


    public Typo(PApplet parentPApplet){
        applet = parentPApplet;
    }

    public Typo(Typo typo_in){
        this(typo_in.applet);
        this.sampledShape = typo_in.getShape();
    }

	/**
	 * a Constructor, usually called in the setup() method in your sketch to
	 * initialize and start the Library.
	 *
	 * @param parentPApplet the parent PApplet
	 */
  public Typo(PApplet parentPApplet, String txt, String font, int text_size, RPoint pos){
      this(parentPApplet); 

    text_size = text_size;
    origShape = RG.getText(txt, font, text_size, RG.CENTER);

    //origShape = orig; 
    RPoint origShape_center = origShape.getCenter();

    // // center the shape on point pos
    double t_x = pos.x - origShape_center.x / 2.0;
    double t_y = pos.y - origShape_center.y;
    origShape.translate((float)t_x, (float)t_y);
  }

  public Typo(PApplet parentPApplet, RShape shp, RPoint pos){
    this(parentPApplet); 

    origShape = shp;
    
    RPoint origShape_center = origShape.getCenter();
    
    // center the shape on point pos
    double t_x = pos.x - origShape_center.x / 2.0;
    double t_y = pos.y - origShape_center.y;
    origShape.translate(0, 0);
  }
  
  public Typo(PApplet parentPApplet, String txt, String font, int text_size, float pos_x, float pos_y){
    this(parentPApplet, txt, font, text_size, new RPoint(pos_x, pos_y));
  }

  public Typo(PApplet parentPApplet, String txt, String font, int text_size, RPoint pos, int anchor){
    this(parentPApplet);

    txt = txt;
    text_size = text_size;
    origShape = RG.getText(txt, font, text_size, anchor);

    RPoint origShape_center = origShape.getCenter();

    // center the shape on point pos
    float t_x = pos.x;
    float t_y = pos.y;
    origShape.translate(t_x, t_y);
  }

  /*
   * Return the sampledShape if it exists, otherwise return original shape 
   */
  public RShape getShape(){
    if (sampledShape == null){
      return origShape;
    }else{
      return sampledShape;
    }
  }

  /*
   * Draw the sampledShape if it exists, otherwise return original shape 
   */
  public void draw(){

    if (sampledShape == null){
      origShape.draw();
    }else{
      sampledShape.draw();
    }
  }

  public RShape resampleByPath(int n_samples_in){
    sampledShape = new RShape();

    for (int i_c = 0; i_c < origShape.countChildren(); i_c ++){
      RShape shape_loc = new RShape();
   
      for (int i_p = 0; i_p < origShape.children[i_c].countPaths(); i_p ++){      
        // ratio of each segments relative to total length
        float frac = 1.0f/n_samples_in;

        // init a list of points
        RPoint[] cur_points = new RPoint[n_samples_in];
        
        for (int ii = 0; ii < cur_points.length; ii ++){
          // get each point
          cur_points[ii] = origShape.children[i_c].paths[i_p].getPoint(frac * ii);
        }
        
        // create path from point
        RPath path_loc = new RPath(cur_points);
        
        // add path to shape
        shape_loc.addPath(path_loc);
      }
      // add shape to main shape as children
      sampledShape.addChild(shape_loc);
    }
    return sampledShape; 
  }

  public RShape sortByLength(){
    RShape sortedShape = new RShape();

    for (int i_c = 0; i_c < sampledShape.countChildren(); i_c++){
      RPath[] paths = sampledShape.children[i_c].paths;
      Arrays.sort(paths, new SortByLength());
      RShape shape_loc = new RShape(paths);
      sortedShape.addChild(shape_loc);
    }

    sampledShape = sortedShape;
    return sampledShape;
  }

  /*
   * Resample the original shape and return sampledShape
   * the shape is transformed into -n_sample_in- straight segment distributed into each paths
   *
   * if sample_by_letter == true, each letter has -n_sample_in- segments
   */
  public RShape resample(int n_samples_in, boolean samples_by_letter){
    samples = n_samples_in;
    
    int loc_n_samples = n_samples_in;
    if (!samples_by_letter){
      loc_n_samples = samples/origShape.countChildren();
    }
    
    sampledShape = new RShape();

    for (int i_c = 0; i_c < origShape.countChildren(); i_c ++){
      RShape shape_loc = new RShape();
      float total_length = origShape.children[i_c].getCurveLength();
      
      for (int i_p = 0; i_p < origShape.children[i_c].countPaths(); i_p ++){
        float l =  origShape.children[i_c].paths[i_p].getCurveLength();
        
        // calculate the number of segment related to relative lengths
        int n_points_loc = (int)(l *  (float)loc_n_samples / total_length);

        // the shape cannot have less than 2 points
        n_points_loc = Math.max(n_points_loc, 2);

        // ratio of each segments relative to total length
        float frac = 1.0f/n_points_loc;

        // init a list of points
        RPoint[] cur_points = new RPoint[n_points_loc];
        
        for (int ii = 0; ii < cur_points.length; ii ++){
          // get each point
          cur_points[ii] = origShape.children[i_c].paths[i_p].getPoint(frac * ii);
        }
        
        // create path from point
        RPath path_loc = new RPath(cur_points);

        // add path to shape
        shape_loc.addPath(path_loc);
      }
      // add shape to main shape as children
      sampledShape.addChild(shape_loc);
    }
    return sampledShape; 
  }

  public float getMaxLength(){
    float max_length = 0.0f;
    for (int i_child = 0; i_child < sampledShape.countChildren(); i_child++){
      for (int i_path = 0; i_path < sampledShape.children[i_child].countPaths(); i_path++){
        max_length = Math.max(max_length, sampledShape.children[i_child].paths[i_path].getCurveLength());
      }
    }
    return max_length;
  }

}

