/**
 * Copyright 2019 Laurent Malys
 *
 * This file is part of TypoMorph.
 *
 * TypoMorph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Geomerative is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TypoMorph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package typomorph; 

import processing.core.*;
import geomerative.*;

public class Typomorph {

  // myParent is a reference to the parent sketch
  public PApplet applet;

  int myVariable = 0;

	public final static String VERSION = "##library.prettyVersion##";


	/**
	 * a Constructor, usually called in the setup() method in your sketch to
	 * initialize and start the Library.
	 *
	 * @param theParent the parent PApplet
	 */
    public Typomorph(PApplet parentApplet) {
		applet = parentApplet;

		welcome();
	}

  public Typo createTypo(String txt, String font, int text_size, RPoint pos){
      return new Typo(applet, txt, font, text_size, pos);
  }
    private void welcome() {
        System.out.println("##library.name## ##library.prettyVersion## by ##author##");
    }


    public String sayHello() {
        applet.ellipse(100, 100, 50, 50);
        return "hello library.";
    }

    /**
     * return the version of the Library.
     *
     * @return String
     */
    public static String version() {
        return VERSION;
    }

    /**
     *
     * @param theA the width of test
     * @param theB the height of test
     */
    public void setVariable(int theA, int theB) {
        myVariable = theA + theB;
    }

    /**
     *
     * @return int
     */
    public int getVariable() {
        return myVariable;
    }
}

