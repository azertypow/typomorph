import java.util.*;
import typomorph.*; 
import geomerative.*;

Typomorph TM;

String font_file ="FreeSans.ttf";

void setup() {
  size(1000, 1000);
  smooth();

  RG.setPolygonizer(RG.UNIFORMSTEP);
  RG.setPolygonizerStep(1);
  RG.init(this);

  TM = new Typomorph(this);
}

void draw() {
  background(0);
  noFill();
  stroke(255);

  strokeWeight(2);
  int n_samples = int(map(mouseX, 0, width, 10, 1000));

  int n_effect = int(map(mouseY, 0, height, 1, 30));

  float s = 180/n_effect;

  Typo typo = TM.createTypo("T", font_file, 800, new RPoint(height/2, width/2));
  typo.resample(n_samples, false);

  TypoWaves test_effect = new TypoWaves(typo, s, 1, 2, 1);

  // typo.draw();

  test_effect.update(); 
  test_effect.drawLines();

  int n_rot = n_effect;

  for (int i = 0; i < n_rot; i++){

    typo = new Typo(this, test_effect.getOutShape(), new RPoint(width/2, height/2));

    test_effect = new TypoWaves(typo, s, 1, 2 + i, 2 + i);

    test_effect.update();
    test_effect.drawLines(); 
  }
  // Typo typo3 = new Typo(this, test_effect2.getOutShape(), new RPoint(400, 400));

  // TypoWaves test_effect3 = new TypoWaves(typo3, 40, 1, 4 , 6);

  // test_effect3.update();
  // test_effect3.drawLines();
}
