import typomorph.*;
import geomerative.*;

import processing.svg.*;

import toxi.util.datatypes.*;

Typomorph TM;

boolean record;

class CirclesWaves extends TypoWaves{
  public CirclesWaves(Typo typo_in, float amp1, float amp2, float rot, float rot_speed){
    super(typo_in, amp1, amp2, rot, rot_speed);
  }

  public void drawBezierCircle(float radius_1, float radius_2, RPoint center, float speed){
    RPoint[] pts_ext = wave_ext.getPoints();
    RPoint[] pts_int = wave_int.getPoints();

    float step = TWO_PI/pts_ext.length;

    float angle_start = speed * millis() / 1000.0f;

    println(pts_ext.length);

    for (int i = 0; i < pts_ext.length; i++){
      RPath n = new RPath(new RPoint(pts_int[i]));

      RPoint p1 = new RPoint(mouseX + radius_1 * cos(-angle_start*1/3 + step*i - 1),
                             mouseY + radius_1 * sin(-angle_start*1/3 + step*i - 1));

      // RPoint p1 = new RPoint(mouseX, mouseY);
      // RPoint p1 = new RPoint(center.x + radius_1 * cos(angle_start), center.y + radius_1 * sin(angle_start));
      RPoint p2 = new RPoint(p1.x + radius_2 * cos(angle_start + step*i*3), 
                             p1.y + radius_2 * sin(angle_start + step*i*3));

      n.addBezierTo(pts_ext[i], p2, p1);
      n.draw();
    }
  }
}

CirclesWaves waves;

void setup(){
  size(1000, 1000);
  smooth();
  noFill();
  RG.setPolygonizer(RG.UNIFORMSTEP);
  RG.setPolygonizerStep(1);
  RG.init(this);
  
  TM = new Typomorph(this);
}

void draw(){
  background(0);
  if (record){
    beginRecord(SVG, "typomorp.svg"); 
  }
  stroke(255, 150);
  strokeWeight(2);
  int n_samples = 150;//int(map(mouseX, 0, width, 10, 500));
  // int rad2 = int(map(mouseX, 0, width, 10, 500));
  // int rad1 = int(map(mouseX, 0, width, 0, 500));
  int rad1 = 700;
  int rad2 = 0;
  int r_bez = int(map(mouseY, 0, height, 10, 400));
  // int r_bez = 400;
  print(n_samples, "\t");
  Typo typo = TM.createTypo("X", "FreeSans.ttf", 400, new RPoint(width*3/4,height*3/4));
  typo.resample(n_samples, false);
  waves = new CirclesWaves(typo, 300, 0, 1, 2);
  waves.update();
  waves.drawBezierCircle(rad1, 0, new RPoint(width/2, height/2), 1);
  if (record){

    record = false;
    endRecord();
  }
}

void keyPressed(){
  if (key == 'r'){
    record = true;
    println("record");
  }
}
